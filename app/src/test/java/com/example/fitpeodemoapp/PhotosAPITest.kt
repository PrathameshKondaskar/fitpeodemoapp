package com.example.fitpeodemoapp

import com.example.fitpeodemoapp.Network.ApiService
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PhotosAPITest {

    lateinit var mockWebServer: MockWebServer
    lateinit var apiService: ApiService


    @Before
    fun setup(){
        mockWebServer = MockWebServer()

        apiService = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(ApiService::class.java)


    }
    @Test
    fun test_getPhotos() = runTest{

        val mockResponse = MockResponse()
        mockResponse.setBody("[]")
        mockWebServer.enqueue(mockResponse)

        val response = apiService.getPhotos()
        mockWebServer.takeRequest()

        Assert.assertEquals(true, response.body()!!.isEmpty())

    }

    @Test
    fun testGetProducts_returnProducts() = runTest{

        val mockResponse = MockResponse()
        val content = Helper().readFileResource("/response.json")
        mockResponse.setBody(content)
        mockResponse.setResponseCode(200)

        mockWebServer.enqueue(mockResponse)

        val response = apiService.getPhotos()
        mockWebServer.takeRequest()

        Assert.assertEquals(false, response.body()!!.isEmpty())
        Assert.assertEquals(2, response.body()!!.size)

    }

    @Test
    fun testGetProducts_returnError() = runTest{

        val mockResponse = MockResponse()

        mockResponse.setBody("Something went wrong")
        mockResponse.setResponseCode(404)

        mockWebServer.enqueue(mockResponse)

        val response = apiService.getPhotos()
        mockWebServer.takeRequest()

        Assert.assertEquals(false, response.isSuccessful)
        Assert.assertEquals(404, response.code())

    }



    @After
    fun tearDown(){
        mockWebServer.shutdown()
    }
}