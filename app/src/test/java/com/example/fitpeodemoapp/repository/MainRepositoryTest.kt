package com.example.fitpeodemoapp.repository

import com.example.fitpeodemoapp.Network.ApiService
import com.example.fitpeodemoapp.Network.ApiServiceImpl
import com.example.fitpeodemoapp.model.PhotosModelItem
import com.example.fitpeodemoapp.util.ApiState
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert.*

import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

class MainRepositoryTest {

    @Mock
    lateinit var apiService : ApiService

    @Mock
    lateinit var apiServiceImpl: ApiServiceImpl

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun test_test_getPhotos_expectedEmptyList() = runTest{

        Mockito.`when`(apiServiceImpl.getPhotos()).thenReturn(Response.success(emptyList()))
        val sut = MainRepository(apiServiceImpl)
        val result = sut.getPhotos()
        Assert.assertEquals(true,result is ApiState.Success)
        Assert.assertEquals(0, result.data!!.size)

    }

    @Test
    fun test_test_getPhotos_expectedPhotoList() = runTest{
        val photosList = listOf<PhotosModelItem>(
            PhotosModelItem(1,1,"","My Pic 1",""),
            PhotosModelItem(1,1,"","My Pic 1","")
        )


        Mockito.`when`(apiServiceImpl.getPhotos()).thenReturn(Response.success(photosList))
        val sut = MainRepository(apiServiceImpl)
        val result = sut.getPhotos()
        Assert.assertEquals(true,result is ApiState.Success)
        Assert.assertEquals(2, result.data!!.size)

    }

    @Test
    fun test_getPhotos_expectedError() = runTest{

        Mockito.`when`(apiServiceImpl.getPhotos()).thenReturn(Response.error(401,"Unauthorized".toResponseBody()))

        val sut= MainRepository(apiServiceImpl)
        val result = sut.getPhotos()
        Assert.assertEquals(true,result is ApiState.Failure)
        Assert.assertEquals("Something went wrong", result.message)

    }

    @After
    fun tearDown() {
    }
}