package com.example.fitpeodemoapp.ViewModel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.fitpeodemoapp.repository.MainRepository
import com.example.fitpeodemoapp.util.ApiState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.*
import org.junit.Assert.*

import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MainViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val testDispatcher = StandardTestDispatcher()


    @Mock
    lateinit var repository : MainRepository

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        Dispatchers.setMain(testDispatcher)
    }

    @Test
    fun test_getPhotos_expectedEmptyList() = runTest{
        Mockito.`when`(repository.getPhotos()).thenReturn(ApiState.Success(emptyList()))
        val sut = MainViewModel(repository)
        sut.getPhotos()
        advanceUntilIdle()
        val result =sut.photosLivedata.getOrAwaitValue()
        Assert.assertEquals(0, result.data!!.size)

    }

    @Test
    fun test_getPhotos_expectedError()= runTest {

        Mockito.`when`(repository.getPhotos()).thenReturn(ApiState.Failure("Something went wrong"))
        val sut = MainViewModel(repository)
        sut.getPhotos()
        advanceUntilIdle()
        val result = sut.photosLivedata.getOrAwaitValue()
        Assert.assertEquals(true,result is ApiState.Failure)
        Assert.assertEquals("Something went wrong", result.message)
    }



    @After
    fun tearDown() {

        Dispatchers.resetMain()
    }
}