package com.example.fitpeodemoapp.Network

import com.example.fitpeodemoapp.model.PhotosModelItem
import retrofit2.Response
import javax.inject.Inject

class ApiServiceImpl @Inject constructor(private val apiService: ApiService) {

    suspend fun getPhotos():Response<List<PhotosModelItem>> = apiService.getPhotos()

}