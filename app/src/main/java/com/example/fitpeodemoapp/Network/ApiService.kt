package com.example.fitpeodemoapp.Network

import com.example.fitpeodemoapp.model.PhotosModel
import com.example.fitpeodemoapp.model.PhotosModelItem
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("photos")
    suspend fun getPhotos():Response<List<PhotosModelItem>>

}