package com.example.fitpeodemoapp.util

import com.example.fitpeodemoapp.model.PhotosModelItem

sealed class ApiState<T>(val data: T? = null, val message: String? = null){

    class Loading<T> : ApiState<T>()
    class Failure<T>(message: String?,data: T? = null) : ApiState<T>(data,message)
    class Success<T>(data: T) : ApiState<T>(data)

}

