package com.example.fitpeodemoapp.ViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.fitpeodemoapp.model.PhotosModelItem
import com.example.fitpeodemoapp.repository.MainRepository
import com.example.fitpeodemoapp.util.ApiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val repository: MainRepository) : ViewModel() {


    private val _photosMutbaleLivedata = MutableLiveData<ApiState<List<PhotosModelItem>>>()
    val photosLivedata: LiveData<ApiState<List<PhotosModelItem>>>
    get() = _photosMutbaleLivedata


    fun getPhotos(){
        viewModelScope.launch {
        val result =repository.getPhotos()
        _photosMutbaleLivedata.postValue(result)
        }
    }


}