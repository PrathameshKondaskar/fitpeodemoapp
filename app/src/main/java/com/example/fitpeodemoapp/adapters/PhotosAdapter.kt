package com.example.fitpeodemoapp.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.fitpeodemoapp.DetailActivity
import com.example.fitpeodemoapp.R
import com.example.fitpeodemoapp.databinding.EachRowBinding
import com.example.fitpeodemoapp.model.PhotosModelItem
import com.squareup.picasso.Picasso


class PhotosAdapter(private var photosList : List<PhotosModelItem>?) : RecyclerView.Adapter<PhotosAdapter.PhotosViewHolder>() {

    private lateinit var binding: EachRowBinding


    class PhotosViewHolder(
        private val binding: EachRowBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(photoItem: PhotosModelItem) {
            binding.photosModelItem = photoItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotosViewHolder {
        binding = DataBindingUtil.inflate<EachRowBinding>(LayoutInflater.from(parent.context), R.layout.each_row,parent,false)
        binding.eventHandler=EventHandler(binding.root.context)
        return PhotosViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return photosList!!.size
    }

    override fun onBindViewHolder(holder: PhotosViewHolder, position: Int) {
//
//        binding.tasks.text = photosList[position].title
//        Picasso.get().load(photosList[position].url).into(binding.imageView);

        val photoItem = photosList?.get(position)
        holder.bind(photoItem!!)
    }

    fun setData(postList: List<PhotosModelItem>?){
        this.photosList = postList
        notifyDataSetChanged()
    }


    class EventHandler(val context: Context){
        fun onClickEvent(view: View ,photoItem: PhotosModelItem){
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("photo", photoItem)
            context.startActivity(intent)
        }
    }

}