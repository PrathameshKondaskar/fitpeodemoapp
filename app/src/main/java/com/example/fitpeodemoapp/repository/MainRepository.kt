package com.example.fitpeodemoapp.repository

import android.annotation.SuppressLint
import com.example.fitpeodemoapp.Network.ApiServiceImpl
import com.example.fitpeodemoapp.model.PhotosModelItem
import com.example.fitpeodemoapp.util.ApiState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class MainRepository  @Inject constructor(private val apiServiceImpl: ApiServiceImpl) {



    suspend fun getPhotos():ApiState<List<PhotosModelItem>>{
        val response = apiServiceImpl.getPhotos()
        return if(response.isSuccessful){
            val responseBody = response.body()
                if(responseBody != null){
                    ApiState.Success(responseBody)
                }else{
                    ApiState.Failure("Something went wrong")
                }
        }else {
            ApiState.Failure("Something went wrong")
        }

    }


}