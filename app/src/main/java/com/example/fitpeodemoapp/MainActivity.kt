package com.example.fitpeodemoapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.fitpeodemoapp.ViewModel.MainViewModel
import com.example.fitpeodemoapp.adapters.PhotosAdapter
import com.example.fitpeodemoapp.databinding.ActivityMainBinding
import com.example.fitpeodemoapp.util.ApiState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var postAdapter: PhotosAdapter
    private val mainViewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<ActivityMainBinding>(this,R.layout.activity_main)
        setContentView(binding.root)
        initRecyclerView()


        collectResultFromApi()
    }
    private fun initRecyclerView() {
        postAdapter = PhotosAdapter(ArrayList())
        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager= LinearLayoutManager(this@MainActivity)
            adapter = postAdapter
        }
    }
    private fun collectResultFromApi() {
        mainViewModel.getPhotos()

        mainViewModel.photosLivedata.observe(this, Observer{

            when(it){

                is ApiState.Success->{

                    binding.recyclerView.isVisible = true
                    binding.progressBar.isVisible = false
                    postAdapter.setData(it.data)

                }

                is ApiState.Loading->{

                    binding.recyclerView.isVisible = false
                    binding.progressBar.isVisible = true
                }

                is ApiState.Failure->{

                    binding.recyclerView.isVisible = false
                    binding.progressBar.isVisible = false

                    Toast.makeText(this@MainActivity,"API Fail "+it.message, Toast.LENGTH_SHORT).show()

                    Log.d("TAG", "onCreate: ${it.message}")
                }

            }
        })

    }
}