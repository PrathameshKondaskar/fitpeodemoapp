package com.example.fitpeodemoapp

import android.database.DatabaseUtils
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.example.fitpeodemoapp.databinding.ActivityDetailBinding
import com.example.fitpeodemoapp.model.PhotosModelItem
import com.squareup.picasso.Picasso

class DetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<ActivityDetailBinding>(this,R.layout.activity_detail)
        setContentView(binding.root)

        val photosModelItem = intent.getSerializableExtra("photo") as? PhotosModelItem
        binding.photosModelItem = photosModelItem





    }
}